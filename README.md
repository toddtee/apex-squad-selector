# Apex Squad Selector
## Discord Bot - For Making Apex Legends Squads

This discord bot will randomly arrange a list of tagged users into squads of three in size, perfect for Apex Legends.

![discordgo](https://camo.githubusercontent.com/285cfc297824859aa2a3b4248be16fd63797e620/687474703a2f2f62776d617272696e2e6769746875622e696f2f646973636f7264676f2f696d672f646973636f7264676f2e706e67) ![sexyflanders](https://img2.wikia.nocookie.net/__cb20141204210243/simpsonstappedout/images/5/58/Tapped_Out_Stupid_Sexy_Flanders.png)


### How to Use
Once installed, in your channel initiate the bot with:

```
/assbot-squadup @user1 @user2, @user3, ...
```

The bot will randonly assign squads of three for all the users listed.

## Built With

* [discordgo](https://github.com/bwmarrin/discordgo) - Go library for discord.
