package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

const token string = "NzA1OTkzNjk3NDY0MTU2Mjcx.Xq1vyQ.4PccPhjaLAVZICI_yKvNSZTDWFo"

var BotID string

func main() {
	dg, err := discordgo.New("Bot " + token)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	u, err := dg.User("@me")

	if err != nil {
		fmt.Println(err.Error())
		return
	}
	BotID = u.ID

	dg.AddHandler(messageHandler)

	err = dg.Open()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Bot is running!")

	<-make(chan struct{})
	return
}

func messageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == BotID {
		return
	}

	var listOfSquads [][]*discordgo.User

	if strings.Contains(m.Content, "/assbot-squadup") && len(m.Mentions) > 0 {

		if len(m.Mentions) < 3 {
			_, _ = s.ChannelMessageSend(m.ChannelID, "Not enough for squad!")
		}

		if len(m.Mentions) >= 3 {
			var r = userRandomizer(m)
			listOfSquads = squadSplitter(r)
		}

		sendSquadMessage(s, m, listOfSquads)
	}
}

func userRandomizer(message *discordgo.MessageCreate) []*discordgo.User {

	var randomMentions = message.Mentions

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(randomMentions), func(i, j int) { randomMentions[i], randomMentions[j] = randomMentions[j], randomMentions[i] })

	return randomMentions

}

func sendSquadMessage(s *discordgo.Session, m *discordgo.MessageCreate, listOfSquads [][]*discordgo.User) {

	for _, squad := range listOfSquads {
		var userList []string
		for _, user := range squad {
			userList = append(userList, "<@"+user.ID+">")
		}
		if len(userList) == 3 {
			var message = fmt.Sprintf("Squad will be: %s", userList)
			_, _ = s.ChannelMessageSend(m.ChannelID, message)
		} else if len(userList) > 0 && len(userList) < 3 {
			var message = fmt.Sprintf("%s will need to find some friends", userList)
			_, _ = s.ChannelMessageSend(m.ChannelID, message)
		}
	}
}

func squadSplitter(userList []*discordgo.User) [][]*discordgo.User {

	var listForCutting = userList
	var listOfSquads [][]*discordgo.User

	for len(listForCutting) >= 3 {
		var squad []*discordgo.User
		squad = append(listForCutting[:3])
		listOfSquads = append(listOfSquads, squad)
		listForCutting = listForCutting[3:]
	}
	if len(listForCutting) < 3 {
		listOfSquads = append(listOfSquads, listForCutting)
	}

	return listOfSquads

}
